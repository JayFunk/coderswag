package com.morderdesign.coderswag.Services

import com.morderdesign.coderswag.Model.Category
import com.morderdesign.coderswag.Model.Product

object DataService {
    val categories = listOf(
        Category("SHIRTS", "shirtimage"),
        Category("HOODIES", "hoodieimage"),
        Category("HATS", "hatimage"),
        Category("DIGITAL", "digitalgoodsimage"),
        Category("SHIRTS", "shirtimage"),
        Category("HOODIES", "hoodieimage"),
        Category("HATS", "hatimage"),
        Category("DIGITAL", "digitalgoodsimage"),
        Category("SHIRTS", "shirtimage"),
        Category("HOODIES", "hoodieimage"),
        Category("HATS", "hatimage"),
        Category("DIGITAL", "digitalgoodsimage")
    )

    val hats = listOf(
        Product("Devslopes Graphic Beanie", "18$", "hat1"),
        Product("Devslopes Hat Black", "20$", "hat2"),
        Product("Devslopes Hat White", "21$", "hat3"),
        Product("Devslopes Hat Snapback", "22$", "hat4")
    )

    val hoodies = listOf(
        Product("Devslopes Hoodie Gray", "28$", "hoodie1"),
        Product("Devslopes Hoodie Red", "32$", "hoodie2"),
        Product("Devslopes Hoodie White", "28$", "hoodie3"),
        Product("Devslopes Hoodie Green", "32$", "hoodie4")
    )

    val shirts = listOf(
        Product("Devslopes Shirt Gray", "18$", "shirt1"),
        Product("Devslopes Shirt Red", "22$", "shirt2"),
        Product("Devslopes Shirt White", "18$", "shirt3"),
        Product("Devslopes Shirt Green", "22$", "shirt4"),
        Product("Devslopes Shirt Yellow", "22$", "shirt5")
    )

    val digitalGoods = listOf<Product>()

    fun getProducts(category: String) : List<Product>{
        return when(category){
            "SHIRTS" -> shirts
            "HATS" -> hats
            "HOODIES" -> hoodies
            else -> digitalGoods
        }
    }
}